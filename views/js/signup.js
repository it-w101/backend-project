// function signUp() {
//     var isValid = validateForm(); // Check form validation
//     if (!isValid) {
//         return; // Stop further execution if validation fails
//     }
    
//     var _data = {
//         username: document.getElementById("fullname").value,
//         email: document.getElementById("email").value,
//         password: document.getElementById("password").value,
//         pw: document.getElementById("confirmPassword").value, 
//     };

//     if (_data.password !== _data.pw) {
//         alert("Passwords don't match!");
//         return;
//     }

//     fetch('/signup', {
//         method: "POST",
//         body: JSON.stringify(_data),
//         headers: { "Content-type": "application/json; charset=UTF-8" }
//     })
//     .then(response => {
//         if (response.status == 201) {
//             window.open("index.html", "_self");
//         }
//     });
// }

// function validateForm() {
//     var fullname = document.getElementById("fullname").value;
//     var email = document.getElementById("email").value;
//     var password = document.getElementById("password").value;
//     var confirmPassword = document.getElementById("confirmPassword").value;

//     // Validate Full Name
//     if (fullname.trim() === "") {
//         alert("Full name is required.");
//         return false;
//     }

//     // Validate Email
//     var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
//     if (email.trim() === "") {
//         alert("Email is required.");
//         return false;
//     } else if (!emailPattern.test(email)) {
//         alert("Please enter a valid email address.");
//         return false;
//     }

//     // Validate Password
//     if (password.trim() === "") {
//         alert("Password is required.");
//         return false;
//     } else if (password.length < 8) {
//         alert("Password must be at least 8 characters long.");
//         return false;
//     }

//     // Validate Confirm Password
//     if (confirmPassword.trim() === "") {
//         alert("Please confirm your password.");
//         return false;
//     } else if (password !== confirmPassword) {
//         alert("Passwords do not match.");
//         return false;
//     }

//     // All validations passed
//     return true;
// }



// Basically to register an admin

function signUp() {
    //data to be  sent to the POSt request
    var _data = {  //get the admin data and store in variable called data
        username : document.getElementById("fullname").value,
        email : document.getElementById("email").value,
        password : document.getElementById("password").value,
        pw : document.getElementById("confirmPassword").value,
    }
    if (_data.password !== _data.pw){
        alert("Password doestnot match!")
        return
    }
    fetch('/signup', {
        method : "POST",
        body : JSON.stringify(_data),
        headers: {"Content-type":"application/json; charset=UTF-8"}
    })
    .then(response => {
        if(response.status == 201) {
            window.open("index.html", "_self")  //to open the login page in same tab
        }
    })
}
