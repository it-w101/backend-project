// Fetch booking list on window load
window.onload = function () {
  fetch('/bookings')
      .then(response => {
          if (!response.ok) {
              throw new Error('Network response was not ok');
          }
          return response.json();
      })
      .then(data => showBookings(data))
      .catch(error => console.error('Error fetching reservations:', error));
};

function showBookings(data) {
  newRow(data);
}

function newRow(reservations) {
  const table = document.getElementById("pendingReqTable");

  reservations.forEach(reservation => {
      if (reservation.status === "pending") {
          const row = table.insertRow(-1);

          const td = [];
          for (let i = 0; i < table.rows[0].cells.length; i++) {
              td[i] = row.insertCell(i);
          }

          td[0].innerHTML = reservation.bookingid;
          td[1].innerHTML = reservation.username;
          td[2].innerHTML = reservation.room;
          td[3].innerHTML = reservation.date;
          td[4].innerHTML = reservation.email;
          td[5].innerHTML = reservation.time;
          td[6].innerHTML = '<button class="btn-1" onclick="approveReq(this)" style="background-color: green; color: white; border:0px; border-radius: 4px;">Approve</button>';
          td[7].innerHTML = '<button class="btn-2" onclick="declineReq(this)" style="background-color: red; color: white; border:0px; border-radius: 4px;">Decline</button>';
      }
  });
}

function approveReq(btn) {
  const row = btn.parentNode.parentNode;
  const bid = row.cells[0].innerHTML;
  const approvedData = { status: "approved" };

  fetch('/booking/' + bid, {
      method: 'PATCH',
      body: JSON.stringify(approvedData),
      headers: { "Content-type": "application/json; charset=utf-8" }
  })
  .then(response => {
      if (response.ok) {
          row.cells[6].innerHTML = 'Approved';
          row.cells[7].innerHTML = '';
      } else {
          alert("Booking request unable to update");
      }
  })
  .catch(error => {
      console.error('Error:', error);
      alert("An error occurred while processing your request");
  });
}

function declineReq(btn) {
  const row = btn.parentNode.parentNode;
  const bid = row.cells[0].innerHTML;
  const declinedData = { status: "declined" };

  fetch('/booking/' + bid, {
      method: 'PATCH',
      body: JSON.stringify(declinedData),
      headers: { "Content-type": "application/json; charset=utf-8" }
  })
  .then(response => {
      if (response.ok) {
          row.cells[6].innerHTML = 'Declined';
          row.cells[7].innerHTML = '';
      } else {
          alert("Booking request unable to update");
      }
  })
  .catch(error => {
      console.error('Error:', error);
      alert("An error occurred while processing your request");
  });
}

function resetForm() {
  document.getElementById("username").value = "";
  document.getElementById("email").value = "";
  document.getElementById("checkInDate").value = "";
  document.getElementById("time").value = "";
}



function logout() {
    fetch('/logout')
    .then(response => {
        if (response.ok) {
            window.open("index.html", "_self")
} else {
throw new Error(response.statusText)
}
}).catch (e => {
alert(e)
})
}


function toggleSidebar() {
    var sidebar = document.getElementById('side_nav');
    sidebar.classList.toggle('show');
  }
