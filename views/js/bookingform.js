console.log(document.getElementById("bookingForm"))
document.querySelector("#bookingForm").addEventListener("submit", (event) => {
    event.preventDefault();
    addBooking();
});

function addBooking() {
    var data = {
        username : document.getElementById("username").value,
        email: document.getElementById("email").value,
        date: document.getElementById("checkInDate").value,
        time: document.getElementById("time").value,
        room: document.getElementById("room").value,
        status: "pending"
    };
    
    // Convert the JavaScript object 'data' to a JSON string
    var jsonData = JSON.stringify(data);
    console.log(jsonData);
    fetch('/booking', {
        method: 'POST',
        body: jsonData, // Use the JSON string as the request body
        headers: { "Content-type": "application/json" }
    }).then(response => {
        if (response.ok) {
            alert("Reservation added successfully!");
            // Redirect to pending request page or any other action
        } else {
            throw new Error(response.status)
        }
    }).catch(error => {
        console.error('Error adding reservation:', error);
        alert("Failed to add reservation. Please try again.");
    });

    resetform();
}



function resetform() {
    document.getElementById("username").value = "";
    document.getElementById("email").value = "";
    document.getElementById("checkInDate").value = "";
    document.getElementById("time").value = "";
    document.getElementById("room").value = "";
}

function cancelReservation() {
    if (confirm('Are you sure you want to cancel the reservation?')) {
        resetform();
    }

}



function toggleSidebar() {
    var sidebar = document.getElementById('side_nav');
    sidebar.classList.toggle('show');
  }
