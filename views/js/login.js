function login() {
    var _data = {
        email : document.getElementById("email").value,
        password : document.getElementById("password").value
    }
    fetch('/login', {
        method: "POST",
        body: JSON.stringify(_data),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(response => {
        if (response.ok) {
            window.open("home.html", "_self")   //sucess log in means as a response display student data
    } else {
        throw new Error(response.statusText)
    }
    }).catch(e => {
        if (e == "Error: Unauthorized") {
            alert(e+ ". Credentials does not match!")
            return
        }
    });
}


function logout() {
    fetch('/logout')
    .then(response => {
        if(response.ok){
            window.open("index.html", "_self")
        }else{
            throw new Error(response.statusText)
        }
    }).catch (e => {
        alert(e)
    })
}
