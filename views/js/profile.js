window.onload = function() {
    fetch('/users')
        .then(response => response.text())
        .then(data => showAllUsers(data));  // Corrected function name
}

function newRow(user) {
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.rows.length); 

    var td = [];
    for (var i = 0; i < table.rows[0].cells.length; i++) {
        td[i] = row.insertCell(i);
    }

    td[0].innerHTML = user.username;
    td[1].innerHTML = user.email;
    td[2].innerHTML = '<input type="button" onclick="deleteRow(this)" value="delete">';
}

function showAllUsers(data) {  // Corrected function name
    const users = JSON.parse(data);
    users.forEach(user => {
        newRow(user);
    });
}

function deleteRow(button) {
    var row = button.parentNode.parentNode;
    row.parentNode.removeChild(row);
}