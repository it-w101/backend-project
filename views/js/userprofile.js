document.addEventListener("DOMContentLoaded", function() {
    const editProfileButton = document.getElementById("editProfileButton");
    const profileForm = document.getElementById("profileForm");
    const usernameInput = document.getElementById("username");
    const emailInput = document.getElementById("email");
    const contactNumberInput = document.getElementById("contactNumber");
    const saveProfileButton = document.getElementById("saveProfileButton");

    // Load saved profile details from localStorage
    loadProfileDetails();

    editProfileButton.addEventListener("click", function() {
        toggleEditableFields();
    });

    function toggleEditableFields() {
        const readOnly = usernameInput.readOnly;
        usernameInput.readOnly = !readOnly;
        emailInput.readOnly = !readOnly;
        contactNumberInput.readOnly = !readOnly;
        if (!readOnly) {
            editProfileButton.innerText = "Edit";
        } else {
            editProfileButton.innerText = "Cancel";
        }
    }

    saveProfileButton.addEventListener("click", function() {
        if (editProfileButton.innerText === "Cancel") {
            toggleEditableFields();
        }

        // Validation
        if (!validateForm()) {
            return;
        }

        // Save profile details to localStorage
        saveProfileDetails();

        console.log("Username:", usernameInput.value);
        console.log("Email:", emailInput.value);
        console.log("Contact Number:", contactNumberInput.value);
        alert("Profile saved successfully!");
    });

    function validateForm() {
        const username = usernameInput.value.trim();
        const email = emailInput.value.trim();
        const contactNumber = contactNumberInput.value.trim();

        // Username validation
        if (username === "") {
            alert("Username cannot be empty");
            return false;
        }

        // Email validation
        if (email === "") {
            alert("Email cannot be empty");
            return false;
        }
        if (!/\S+@\S+\.\S+/.test(email)) {
            alert("Invalid email address");
            return false;
        }

        // Contact number validation
        if (!/^((17)|(77))\d{6}$/.test(contactNumber)) {
            alert("Contact number must start with 17 or 77 and have 8 digits in total");
            return false;
        }

        return true;
    }

    function saveProfileDetails() {
        const profileDetails = {
            username: usernameInput.value.trim(),
            email: emailInput.value.trim(),
            contactNumber: contactNumberInput.value.trim()
        };
        localStorage.setItem("profileDetails", JSON.stringify(profileDetails));
    }

    function loadProfileDetails() {
        const savedProfileDetails = localStorage.getItem("profileDetails");
        if (savedProfileDetails) {
            const profileDetails = JSON.parse(savedProfileDetails);
            usernameInput.value = profileDetails.username;
            emailInput.value = profileDetails.email;
            contactNumberInput.value = profileDetails.contactNumber;
        }
    }
});

var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];
var modal = document.getElementById("myModal");

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// dashboard.js

// Function to handle edit profile button click
function editProfile() {
    // Retrieve the elements containing the admin details
    var nameElement = document.querySelector('.profile-info ul li:nth-child(1)');
    var emailElement = document.querySelector('.profile-info ul li:nth-child(2)');
    
    // Extract the current name and email values
    var currentName = nameElement.textContent.split(':')[1].trim();
    var currentEmail = emailElement.textContent.split(':')[1].trim();
    
    // Prompt the user to enter the new name and email
    var newName = prompt('Enter your new name:', currentName);
    var newEmail = prompt('Enter your new email:', currentEmail);
    
    // Update the DOM with the new name and email
    nameElement.textContent = 'Name: ' + newName;
    emailElement.textContent = 'Email: ' + newEmail;
    
    // Optionally, you can also send an AJAX request to update the profile details on the server
  }
  // Function to handle delete user button click
  function deleteUser(button) {
    // Retrieve the list item containing the user
    var listItem = button.closest('li');
    
    // Remove the list item from the DOM
    listItem.remove();
    
    // Optionally, you can also send an AJAX request to delete the user from the server
  }
  // Function to handle view details button click
  function viewDetails(button) {
    // Retrieve the list item containing the user
    var listItem = button.closest('li');
    
    // Retrieve the user's name from the list item
    var userName = listItem.querySelector('span').textContent;
    
    // Display a modal or an alert with the user's details
    alert('Details of ' + userName );
    
    // Optionally, you can fetch additional details from the server and display them
  }
  
  
 
  document.addEventListener("DOMContentLoaded", function() {
      var userTableBody = document.querySelector("#userTable tbody");
      userTableBody.innerHTML = ""; // Clear existing table body
  
      // Loop through Local Storage items and create table rows for each user
      for (var i = 0; i < localStorage.length; i++) {
          var key = localStorage.key(i);
          var userData = JSON.parse(localStorage.getItem(key));
  
          var row = document.createElement("tr");
          row.innerHTML = "<td>" + userData.fullname + "</td>" +
                          "<td>" + key + "</td>" +
                          
                          "<td>" +
                              "<button class='btn btn-danger' onclick='deleteUser(this)'>Delete</button>" +
                              "<button class='btn btn-info' onclick='viewDetails(this) id='showEmail'>View Details</button>" +
                          "</td>";
          userTableBody.appendChild(row);
      }
  });
  
  // Function to delete a user
  function deleteUser(button) {
      var row = button.closest("tr");
      var email = row.querySelector("td:nth-child(2)").textContent;
      localStorage.removeItem(email); // Remove user from Local Storage
      row.remove(); // Remove the row from the table
  }
  
  // Function to view user details

    
  function viewDetails(button) {
    var row = button.closest("tr");
    var email = row.querySelector("td:nth-child(2)").textContent;
    var userData = JSON.parse(localStorage.getItem(email));
    var emailColumn = document.getElementById("email_hide");

    // Toggle the display of the email column
    emailColumn.style.display = emailColumn.style.display === "none" ? "table-cell" : "none";

    // If you want to display additional user details, you can use them from the userData object
    // For example, you can display the user's full name:
    alert("Name: " + userData.fullname + "\nEmail: " + email);
}
