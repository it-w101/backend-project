window.onload = function () {
    getBookings(); // Call getReservations function when the window loads
  }
  
  function getBookings() {
    // Send GET request to retrieve all reservations
    fetch('/bookings', {
            method: 'GET',
            headers: {
                "Content-type": "application/json; charset=utf-8"
            }
        })
        .then(response => {
            if (response.ok) {
                return response.json(); // Parse response body as JSON
            } else {
                throw new Error('Unable to retrieve booking records');
            }
        })
        .then(data => {
            // Filter out the approved reservations
            const approvedReservations = data.filter(reservation => reservation.status === 'approved');
            // Display the approved reservations in the table
            showBookingTable(approvedReservations);
  
            const declinedReservations = data.filter(reservation => reservation.status === 'declined')
            showDeclinedRecords(declinedReservations)
            
        })
        .catch(error => {
            // Handle network errors or other exceptions
            console.error('Error:', error);
            alert("An error occurred while retrieving reservations");
        });
  }
  
  function showBooking(data) {
    const reservation = JSON.parse(data) //converting from JSON
    newRow(reservation)
  }
  
  function showBookingTable(reservations) {
    // Find the <table> element:
    var table = document.getElementById("bookingrecords");
    if (!table) {
        console.error("Table element with class 'bookingrecords' not found.");
        return;
    }
  
    reservations.forEach(reservation => {
        newRow(table, reservation);
    });
  }
  
  
  function showDeclinedRecords(reservations) {
      // Find the <table> element:
      var table = document.getElementById("declinedrecords");
      if (!table) {
          console.error("Table element with class 'declinedrecords' not found.");
          return;
      }
  
      reservations.forEach(reservation => {
          newRow(table, reservation);
      });
    }
  
  
  
  function newRow(table, reservation) {
    var row = table.insertRow();
    var cells = [];
  
    for (var i = 0; i < 8; i++) {
        cells[i] = row.insertCell(i);
    }
  
    // Create a checkbox in the first cell:
    var checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.classList.add('delete-checkbox');
    checkbox.dataset.bookingId = reservation.bookingid;  // Attach booking ID to checkbox
    checkbox.addEventListener('click', function() {
        handleCheckboxClick(checkbox);
    });
    cells[0].appendChild(checkbox);
  
    // Fill in the other cells with reservation data:
    cells[1].textContent = reservation.bookingid;
    cells[2].textContent = reservation.username;
    cells[3].textContent = reservation.room;
    cells[4].textContent = reservation.date;
    cells[5].textContent = reservation.email;
    cells[6].textContent = reservation.time;
    cells[7].textContent = reservation.status;
  }
  
  var selectedBookingIds = []; 
  function handleCheckboxClick(checkbox) {
      var bookingId = checkbox.dataset.bookingId;
      if (checkbox.checked) {
          selectedBookingIds.push(bookingId);
      } else {
          var index = selectedBookingIds.indexOf(bookingId);
          if (index !== -1) {
              selectedBookingIds.splice(index, 1); 
          }
      }
  }
  
  // Function to delete selected records
  function deleteSelectedRecords() {
  
      if (selectedBookingIds.length === 0) {
          alert('At least choose one booking record to delete.');
          return;
      }
  
      if (confirm('Are you sure you want to DELETE the selected records?')) {
          selectedBookingIds.forEach(function(bookingId) {
              var row = document.querySelector('input[data-booking-id="' + bookingId + '"]').closest('tr');
  
              // Delete from backend
              fetch('/booking/' + bookingId, {
                  method: "DELETE",
                  headers: {"Content-type": "application/json; charset=UTF-8"}
              }).then(response => {
                  if (response.ok) {
                      // Delete row from table
                      row.parentNode.removeChild(row);
                  } else {
                      console.error('Failed to delete record with ID:', bookingId);
                  }
              }).catch(error => {
                  console.error('Error deleting record:', error);
              });
          });
  
          // Clear the array after deletion
          selectedBookingIds = [];
      }
  }
  
  // Function to handle the "Select All" button click event
  document.querySelector("#select-all").addEventListener("click", function() {
      // Get all checkboxes with the class 'delete-checkbox'
      var checkboxes = document.querySelectorAll(".delete-checkbox");
      var selectAllButton = document.querySelector("#select-all");
  
      // Check if the "Select All" button is currently set to select all or deselect all
      var selectAll = selectAllButton.textContent === "Select All";
      
      checkboxes.forEach(function(checkbox) {
          checkbox.checked = selectAll;
          handleCheckboxClick(checkbox); // Update the selectedBookingIds array
      });
  
      // Toggle the button text
      selectAllButton.textContent = selectAll ? "Deselect All" : "Select All";
  });
  
  function handleCheckboxClick(checkbox) {
      var bookingId = checkbox.dataset.bookingId;
      if (checkbox.checked) {
          if (!selectedBookingIds.includes(bookingId)) {
              selectedBookingIds.push(bookingId);
          }
      } else {
          var index = selectedBookingIds.indexOf(bookingId);
          if (index !== -1) {
              selectedBookingIds.splice(index, 1);
      }
    }
  }
  