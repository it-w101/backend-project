const sidebarToggle = document.querySelector("#sidebar-toggle");
sidebarToggle.addEventListener("click",function(){
    document.querySelector("#sidebar").classList.toggle("collapsed");
});

window.onload = function () {
    getBookings(); // Call getReservations function when the window loads
  }
  
  function getBookings() {
    // Send GET request to retrieve all reservations
    fetch('/bookings', {
            method: 'GET',
            headers: {
                "Content-type": "application/json; charset=utf-8"
            }
        })
        .then(response => {
            if (response.ok) {
                return response.json(); // Parse response body as JSON
            } else {
                throw new Error('Unable to retrieve booking records');
            }
        })
        .then(data => {
            // Filter out the approved reservations
            const approvedReservations = data.filter(reservation => reservation.status === 'approved');
            // Display the approved reservations in the table
            showBookingTable(approvedReservations);
        })
        .catch(error => {
            // Handle network errors or other exceptions
            console.error('Error:', error);
            alert("An error occurred while retrieving reservations");
        });
  }
  
  function showBooking(data) {
    const reservation = JSON.parse(data) //converting from JSON
    newRow(reservation)
  }
  
  function showBookingTable(reservations) {
    // Find the <table> element:
    var table = document.getElementById("approvedRecords");
    if (!table) {
        console.error("Table element with class 'bookingrecords' not found.");
        return;
    }
  
    reservations.forEach(reservation => {
        newRow(table, reservation);
    });
  }
  
  
  function newRow(table, reservation) {
    var row = table.insertRow();
    var cells = [];
  
    for (var i = 0; i < 6; i++) {
        cells[i] = row.insertCell(i);
    }
  
    // Fill in the other cells with reservation data:
    cells[0].textContent = reservation.username;
    cells[1].textContent = reservation.email;
    cells[2].textContent = reservation.room;
    cells[3].textContent = reservation.date;
    cells[4].textContent = reservation.time;
    cells[5].textContent = reservation.status;
  }
