package main

import (
	"admin/admin/routes"
	"admin/admin/utils"
	"log"
	"net/http"
)

func main() {
	if err := utils.LoadEnvFile(".env"); err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}

	router := routes.InitializeRoutes()
	log.Println("Application running on port 8085")
	log.Fatal(http.ListenAndServe(":8085", router))
}
