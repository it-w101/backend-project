module admin

go 1.21.7

require (
	github.com/gorilla/mux v1.8.1
	github.com/lib/pq v1.10.9
	github.com/stretchr/testify v1.9.0
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)


require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	// github.com/sendgrid/rest v2.6.9+incompatible // indirect
	// github.com/sendgrid/sendgrid-go v3.14.0+incompatible // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
