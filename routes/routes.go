package routes

import (
	"admin/admin/controller"
	"net/http"

	"github.com/gorilla/mux"
)

// InitializeRoutes sets up the routes for the application and returns the router
func InitializeRoutes() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/signup", controller.UserSignup).Methods("POST")
	router.HandleFunc("/login", controller.UserLogin).Methods("POST")
	router.HandleFunc("/logout", controller.Logout)
	router.HandleFunc("/user/{email}", controller.GetUser).Methods("GET")
	router.HandleFunc("/users", controller.GetAllUsers).Methods("GET")
	router.HandleFunc("/user/{email}", controller.DeleteUser).Methods("DELETE")

	// bookings
	router.HandleFunc("/booking", controller.AddBookings).Methods("POST")
	router.HandleFunc("/booking/{bid}", controller.GetBooking).Methods("GET")
	router.HandleFunc("/bookings", controller.GetAllBookings)
	router.HandleFunc("/booking/{bid}", controller.DeleteBookings).Methods("DELETE")
	router.HandleFunc("/booking/{bookingid}", controller.UpdateStatus).Methods("PATCH")

	// ADMIN
	router.HandleFunc("/adminlogin", controller.AdminLogin).Methods("POST")
	router.HandleFunc("/adminsignup", controller.AdminSignup).Methods("POST")
	router.HandleFunc("/adminlogout", controller.Logout)
	router.HandleFunc("/admin/{email}", controller.GetAdmin).Methods("GET")
	router.HandleFunc("/admin/{id}", controller.UpdateAdmin).Methods("PUT")
	router.HandleFunc("/admin/{id}", controller.DeleteAdmin).Methods("DELETE")

	// views
	// Frontend- gives static files
	fhandler := http.FileServer(http.Dir("./views"))
	router.PathPrefix("/").Handler(fhandler)

	return router
}
