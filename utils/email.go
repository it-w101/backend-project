package utils

import (
	"log"
	"os"

	"gopkg.in/gomail.v2"
)

func SendStatusEmail(toEmail, username, room, date, time, status string) {
	fromEmail := os.Getenv("SMTP_EMAIL")
	fromPassword := os.Getenv("SMTP_PASSWORD")

	m := gomail.NewMessage()
	m.SetHeader("From", fromEmail)
	m.SetHeader("To", toEmail)
	m.SetHeader("Subject", "Your booking status has been"+status)
	m.SetBody("text/plain", "Dear "+username+",\n\nYour booking for "+room+" on "+date+" at "+time+" has been "+status+".\n\nBest regards,\nEvent Scheduling System")

	d := gomail.NewDialer("smtp.gmail.com", 587, fromEmail, fromPassword)

	if err := d.DialAndSend(m); err != nil {
		log.Println("Error sending email:", err)
	} else {
		log.Println("Email sent successfully to", toEmail)
	}
}
