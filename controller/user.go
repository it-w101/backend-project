package controller

import (
	// "fmt"
	"admin/admin/model"
	"admin/admin/utils/httpResp"
	"database/sql"
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func UserSignup(w http.ResponseWriter, r *http.Request) {
	var user model.User
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		httpResp.RespondWithJSON(w, http.StatusBadRequest, "invalid json body")
		return
	}

	defer r.Body.Close()
	saveErr := user.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "user added"})

}

func UserLogin(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	getErr := user.Get()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "success"})

}

func UserLogout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "cookie",
		Expires: time.Now(),
	})

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "session ended"})
}

func VerifyCookieUser(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("cookie")
	if err != nil {
		if err == http.ErrNoCookie {
			httpResp.RespondWithError(w, http.StatusSeeOther, "cookie not found")

			return false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError, "internal server error")
		return false
	}

	if cookie.Value != "user logged in" {
		httpResp.RespondWithError(w, http.StatusUnauthorized, "cookie does not match")
		return false

	}
	return true
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	s := model.User{Email: email} //convert int to student of type struct
	getErr := s.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows: //if that row is not there
			httpResp.RespondWithError(w, http.StatusNotFound, "User not found")

		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())

		}
		return

	}
	httpResp.RespondWithJSON(w, http.StatusOK, s)

}

// Delete
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]

	s := model.User{Email: email}
	if err := s.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "User deleted"})

}

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	students, getErr := model.GetAllUsers()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, students)
}
