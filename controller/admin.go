package controller

import (
	"admin/admin/model"
	"admin/admin/utils/httpResp"
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func AdminSignup(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}

	defer r.Body.Close()
	saveErr := admin.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "admin added"})

}

func AdminLogin(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	getErr := admin.Get()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "success"})
}

func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "cookie",
		Expires: time.Now(),
	})

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"message": "session ended"})
}

// UpdateAdmin updates the details of an existing admin
func UpdateAdmin(w http.ResponseWriter, r *http.Request) {
	// Parse the request body to extract updated admin details
	var updatedAdmin model.Admin
	err := json.NewDecoder(r.Body).Decode(&updatedAdmin)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Implement any validation logic here if needed

	// Update the admin details in the database
	updateErr := updatedAdmin.Update()
	if updateErr != nil {
		http.Error(w, updateErr.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	http.Error(w, "Admin details updated successfully", http.StatusOK)
}

// DeleteAdmin deletes an admin account
func DeleteAdmin(w http.ResponseWriter, r *http.Request) {
	// Implement delete logic here
}

// GetAdmin retrieves details of a specific admin
func GetAdmin(w http.ResponseWriter, r *http.Request) {
	// Extract the email parameter from the URL path
	vars := mux.Vars(r)
	email := vars["email"]
	if email == "" {
		httpResp.RespondWithError(w, http.StatusBadRequest, "email parameter is required")
		return
	}

	// Retrieve the admin details from the database using the provided email
	admin := model.Admin{Email: email}
	if err := admin.Get(); err != nil {
		httpResp.RespondWithError(w, http.StatusNotFound, "admin not found")
		return
	}

	// Serialize the admin details into JSON format
	adminJSON, err := json.Marshal(admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, "failed to serialize admin details")
		return
	}

	// Write the JSON response to the HTTP response writer
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(adminJSON)
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("cookie")
	if err != nil {
		if err == http.ErrNoCookie {
			httpResp.RespondWithError(w, http.StatusSeeOther, "cookie not found")

			return false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError, "internal server error")
		return false
	}

	if cookie.Value != "admin logged in" {
		httpResp.RespondWithError(w, http.StatusUnauthorized, "cookie does not match")
		return false

	}
	return true
}
