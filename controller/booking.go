package controller

import (
	"admin/admin/model"
	"admin/admin/utils"
	"admin/admin/utils/httpResp"
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddBookings(w http.ResponseWriter, r *http.Request) {
	var book model.Booking
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&book); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	saveErr := book.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"status": "booking added"})

}

func GetBooking(w http.ResponseWriter, r *http.Request) {
	bid := mux.Vars(r)["bid"]
	bookingId, idErr := getBookingId(bid)

	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	b := model.Booking{BookingId: bookingId}
	if getErr := b.Read(); getErr != nil { // Corrected the assignment and condition
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Records not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, b)
}

func getBookingId(bIdParam string) (int64, error) {
	bookId, bookErr := strconv.ParseInt(bIdParam, 10, 64)
	if bookErr != nil {
		return 0, bookErr
	}
	return bookId, nil
}

// Get all bookings
func GetAllBookings(w http.ResponseWriter, r *http.Request) {
	bookings, getErr := model.GetAllBookngs()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, bookings)

}

func DeleteBookings(w http.ResponseWriter, r *http.Request) {
	bid := mux.Vars(r)["bid"]
	bookingID, idErr := getBookingId(bid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	b := model.Booking{BookingId: bookingID}
	if err := b.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})

}

func UpdateStatus(w http.ResponseWriter, r *http.Request) {
	var requestBody struct {
		Status string `json:"status"`
	}
	if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid request body")
		return
	}

	bid := mux.Vars(r)["bookingid"]
	bookingId, idErr := getBookingId(bid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	b := model.Booking{BookingId: bookingId}
	if getErr := b.Read(); getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Booking records not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}

	if updateErr := b.UpdateStatus(requestBody.Status); updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Booking records not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
		return
	}

	var responseMessage string
	if requestBody.Status == "approved" {
		responseMessage = "approved"
	} else if requestBody.Status == "declined" {
		responseMessage = "declined"
	} else {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid status value")
		return
	}

	// Send email notification
	utils.SendStatusEmail(b.Email, b.UserName, b.Room, b.Date, b.Time, requestBody.Status)

	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": responseMessage})
}
