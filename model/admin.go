package model

import "admin/admin/dataStore/postgres"

type Admin struct {
	UserName string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

const queryInsertAdmin = "INSERT INTO admin(username, email, password) VALUES ($1, $2, $3) RETURNING email;"

func (adm *Admin) Create() error {
	row := postgres.Db.QueryRow(queryInsertAdmin, adm.UserName, adm.Email, adm.Password)
	err := row.Scan(&adm.Email)
	return err
}

const queryGetAdmin = "SELECT email, password FROM admin WHERE email=$1 and password=$2;"

func (adm *Admin) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.Email, adm.Password).Scan(&adm.Email, &adm.Password)
}

// Update updates the details of an existing admin in the database

func (adm *Admin) Update() error {
	// Prepare the SQL statement for updating admin details
	query := `
        UPDATE admin
        SET username = $1, email = $2, password = $3
        WHERE email = $4
    `

	// Execute the SQL statement to update admin details
	_, err := postgres.Db.Exec(query, adm.UserName, adm.Email, adm.Password, adm.Email)
	if err != nil {
		return err
	}

	return nil
}
