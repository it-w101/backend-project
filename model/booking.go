package model

import (
	"admin/admin/dataStore/postgres"
	"fmt"
)

type Booking struct {
	BookingId int64  `json:"bookingid"`
	UserName  string `json:"username"`
	Email     string `json:"email"`
	Date      string `json:"date"`
	Time      string `json:"time"`
	Room      string `json:"room"`
	Status    string `json:"status"`
}

const queryInsertBooking = "INSERT INTO bookings(username, email, date, time, room, status) values ($1, $2, $3, $4, $5, $6);"

func (bks *Booking) Create() error {
	_, err := postgres.Db.Exec(queryInsertBooking, bks.UserName, bks.Email, bks.Date, bks.Time, bks.Room, bks.Status)
	return err
}

// Get
const queryGetBooking = "SELECT bookingid, username, email, date, time, room, status FROM bookings WHERE bookingid=$1;"

func (b *Booking) Read() error {
	return postgres.Db.QueryRow(queryGetBooking, b.BookingId).Scan(&b.BookingId, &b.UserName, &b.Email, &b.Date, &b.Time, &b.Room, &b.Status)
}

// Getall the users
func GetAllBookngs() ([]Booking, error) {
	rows, getErr := postgres.Db.Query("SELECT *from bookings;")

	if getErr != nil {
		return nil, getErr
	}

	bookings := []Booking{}

	for rows.Next() {
		var b Booking
		dbErr := rows.Scan(&b.BookingId, &b.UserName, &b.Email, &b.Date, &b.Time, &b.Room, &b.Status)

		if dbErr != nil {
			return nil, dbErr
		}
		bookings = append(bookings, b)
	}

	rows.Close()
	return bookings, nil

}

const queryDeleteBookings = "DELETE FROM bookings WHERE bookingid=$1 RETURNING bookingid;"

func (b *Booking) Delete() error {
	if err := postgres.Db.QueryRow(queryDeleteBookings, b.BookingId).Scan(&b.BookingId); err != nil {
		return err
	}
	return nil
}

const queryUpdateStatus = "UPDATE bookings SET status = $1 WHERE bookingid = $2"

func (b *Booking) UpdateStatus(status string) error {
	_, err := postgres.Db.Exec(queryUpdateStatus, status, b.BookingId)
	if err != nil {
		fmt.Println("Error executing UPDATE query:", err)
		return err
	}
	fmt.Println("UPDATE query executed successfully")
	return nil
}
