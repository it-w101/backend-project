package model

import "admin/admin/dataStore/postgres"

type User struct {
	UserName string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

const queryInsertUser = "INSERT INTO userList(username, email, password) VALUES ($1, $2, $3) RETURNING email;"

func (usr *User) Create() error {
	row := postgres.Db.QueryRow(queryInsertUser, usr.UserName, usr.Email, usr.Password)
	err := row.Scan(&usr.Email)
	return err

}

const queryGetUser = "SELECT email, password FROM userList WHERE email=$1 and password=$2;"

func (usr *User) Get() error {
	return postgres.Db.QueryRow(queryGetUser, usr.Email, usr.Password).Scan(&usr.Email, &usr.Password)
}

const queryShowUser = "SELECT username, email, password FROM userList WHERE email=$1;"

// reads the datas from database
func (s *User) Read() error {
	return postgres.Db.QueryRow(queryShowUser, s.Email).Scan(&s.UserName, &s.Email, &s.Password)

}

// delete
const queryDeleteUser = "DELETE FROM userList WHERE email=$1 RETURNING email;"

func (s *User) Delete() error {
	if err := postgres.Db.QueryRow(queryDeleteUser, s.Email).Scan(&s.Email); err != nil {
		return err
	}
	return nil
}

// Get all the student datas

func GetAllUsers() ([]User, error) {
	rows, getErr := postgres.Db.Query("SELECT * from userList;")
	if getErr != nil {
		return nil, getErr
	}

	users := []User{}

	for rows.Next() {
		var s User
		dbErr := rows.Scan(&s.Email, &s.UserName, &s.Password)
		if dbErr != nil {
			return nil, dbErr
		}

		users = append(users, s)
	}
	rows.Close()
	return users, nil

}
