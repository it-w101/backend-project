document.addEventListener('DOMContentLoaded', function () {
    var forgotPasswordLink = document.getElementById('forgot-password-link');
    var resetEmailOverlay = document.getElementById('reset-email-overlay');
    var resetPasswordOverlay = document.getElementById('reset-password-overlay');
    var resetSuccessOverlay = document.getElementById('reset-success-overlay');
  
    forgotPasswordLink.addEventListener('click', function (event) {
      event.preventDefault();
      resetEmailOverlay.style.display = 'block';
    });
  
    var resetEmailForm = document.getElementById('reset-email-form');
    var resetPasswordForm = document.getElementById('reset-password-form');
    var newPasswordInput = document.getElementById('new-password');
    var confirmPasswordInput = document.getElementById('confirm-password');
  
    resetEmailForm.addEventListener('submit', function (event) {
      event.preventDefault();
      // Simulate sending email and receiving reset link
      resetEmailOverlay.style.display = 'none';
      resetPasswordOverlay.style.display = 'block'; // Display the reset password form
    });
  
    resetPasswordForm.addEventListener('submit', function (event) {
      event.preventDefault();
      var newPassword = newPasswordInput.value;
      var confirmPassword = confirmPasswordInput.value;
  
      // Password validation
      if (newPassword.length < 8 ) {
        alert('Password must be at least 8 characters long.');
        return;
      }
  
      if (newPassword !== confirmPassword) {
        alert('Passwords do not match!');
        return;
      }
  
      resetPasswordOverlay.style.display = 'none'; 
      resetSuccessOverlay.style.display = 'block'; 
    });
  
    document.getElementById("login-button").addEventListener("click", function() {
      window.location.href = "signin.html"; // Redirect to the login page
    });
  
    var closeResetEmailBtn = document.getElementById('close-reset-email');
    var closeResetPasswordBtn = document.getElementById('close-reset-password');
  
    closeResetEmailBtn.addEventListener('click', function () {
      resetEmailOverlay.style.display = 'none';
    });
  
    closeResetPasswordBtn.addEventListener('click', function () {
      resetPasswordOverlay.style.display = 'none';
    });
});
